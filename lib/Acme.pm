package Acme;

=head1 NAME

Acme - An example class

=head1 VERSION

This documentation refers to Acme version 1.0.0.

=cut

our $VERSION = 1.0.0;

=head1 SYNOPSIS

    use Acme;

    my $obj = Acme->new;

=head1 DESCRIPTION

An example class with a relatively full representation of POD
documentation so that other tooling can transform, process, and
display it.

You can do many things.  See L<perlpod> for more information.

There is some simple markup that can be done.

I<italics> is possible, as is B<bold>.

C<code> is also avaiable.

You can even do hyperlinks.

=over

=item
L<Net::Ping>

=item
L<perlsyn/"For Loops">

=item
L<Perl Error message|perldiag>

=item
L</"SUBROUTINES">

=item
L<http://metacpan.org/>

=item
L<MetaCPAN|http://metacpan.org/>

=back

=cut


use strict;
use warnings;

=head1 METHODS

=cut

=head2 new

Creates a new Acme object.

Example:

    my $acme = Acme->new;

=cut

sub new {
    my $class = shift;

    return bless {}, $class;
}

=head1 AUTHOR

Joshua T. Rowe <jrowe@jrowe.org>

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2022 Joshua T. Rowe <jrowe@jrowe.org>.

This module is free software; you can redistribute it and/or modify it under
the same terms as Perl itself. See L<perlartistic>.  This program is
distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.

=cut

1;
