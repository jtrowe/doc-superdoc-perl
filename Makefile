
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --warn-undefined-variables

.DELETE_ON_ERROR:

build_dir=build


.PHONEY : perldoc
perldoc : \
$(build_dir)/perldoc/html/Acme.html \
$(build_dir)/perldoc/markdown/Acme.md \
$(build_dir)/perldoc/nroff/Acme.nroff \
$(build_dir)/perldoc/pod/Acme.pod \
$(build_dir)/perldoc/strip/Acme.pm \
$(build_dir)/perldoc/text/Acme.txt \
$(build_dir)/perldoc/xhtml/Acme.html \
$(build_dir)/perldoc/xml/Acme.xml \
$(build_dir)/pod2docbook/docbook4.2/Acme.sgml \
$(build_dir)/Perl/Perl/Acme.pm


$(build_dir)/Perl/Perl/Acme.pm : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	cp --archive $< $@


$(build_dir)/perldoc/html/Acme.html : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -d $@ -ohtml $<


$(build_dir)/perldoc/markdown/Acme.md : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -d $@ -omarkdown $<


$(build_dir)/perldoc/nroff/Acme.nroff : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -d $@ -onroff $<


$(build_dir)/perldoc/pod/Acme.pod : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -d $@ -u $<


$(build_dir)/perldoc/strip/Acme.pm : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -d $@ -ostrip $<
	perltidy -b $@


$(build_dir)/perldoc/text/Acme.txt : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -d $@ -t $<


$(build_dir)/perldoc/xhtml/Acme.html : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -d $@ -oxhtml $<


$(build_dir)/perldoc/xml/Acme.xml : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	perldoc -oxml -T $< > $@


$(build_dir)/pod2docbook/docbook4.2/Acme.sgml : lib/Acme.pm
	mkdir --parents $(shell dirname $@)
	carton install
	carton exec -- pod2docbook --doctype=section $< $@ 2> $(shell dirname $@)/err.log


